package com.example.rafaelgarcia.rebtelassignmentkt

import android.app.Activity
import android.os.Bundle
import android.view.KeyEvent
import com.example.rafaelgarcia.rebtelassignmentkt.countries.CountriesActivity
import com.example.rafaelgarcia.rebtelassignmentkt.utils.Constants
import com.example.rafaelgarcia.rebtelassignmentkt.utils.RuntimeUtils
import kotlinx.android.synthetic.main.activity_welcome.*

class WelcomeActivity : Activity(), WelcomeView.Listener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)

        (welcome_view_layout_id as WelcomeView).setListener(this)

    }

    override fun onStartRequested() {
        CountriesActivity.startCountriesActivity(this)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            RuntimeUtils.displayWarningDialog(this, Constants.WARNING_DIALOG_TITLE, Constants.WARNING_DIALOG_MESSAGE);
        }
        return true;
    }
}


package com.example.rafaelgarcia.rebtelassignmentkt

import android.content.Context
import android.util.AttributeSet
import android.widget.RelativeLayout
import kotlinx.android.synthetic.main.welcome_view.view.*

class WelcomeView(context: Context?, attrs: AttributeSet?) : RelativeLayout(context, attrs) {

    private lateinit var listener: Listener

    interface Listener {

        fun onStartRequested();

    }

    fun setListener(viewListener: Listener) {
        listener = viewListener
    }

    override fun onFinishInflate() {
        super.onFinishInflate()

        btn_access.setOnClickListener({ listener.onStartRequested() })


    }

}

package com.example.rafaelgarcia.rebtelassignmentkt.api

import com.example.rafaelgarcia.rebtelassignmentkt.api.service.CountryService

class ApiManager private constructor() {

    private object Holder {
        val INSTANCE = ApiManager()
    }

    companion object {
        val instance: ApiManager by lazy { Holder.INSTANCE }
    }

    fun getCountryService(): CountryService {
        return ServiceFactory.createRetrofitService(CountryService::class.java)
    }

}

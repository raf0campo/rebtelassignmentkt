package com.example.rafaelgarcia.rebtelassignmentkt.api

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class ServiceFactory private constructor () {

    companion object {

        @JvmStatic
        private val SERVICE_ENDPOINT = "https://restcountries.eu/"

        /**
         * Creates a retrofit service from an arbitrary class (clazz)
         * @param clazz interface of the retrofit service
         * @return retrofit service with defined endpoint
         */
        fun <T> createRetrofitService(clazz: Class<T>): T {
            val restAdapter = Retrofit.Builder()
                    .baseUrl(SERVICE_ENDPOINT)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()
            return restAdapter.create(clazz)
        }

    }

}

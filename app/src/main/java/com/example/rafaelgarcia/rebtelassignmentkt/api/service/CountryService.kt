package com.example.rafaelgarcia.rebtelassignmentkt.api.service

import com.example.rafaelgarcia.rebtelassignmentkt.countries.model.Country
import io.reactivex.Single
import retrofit2.http.GET

/**
 * The calls will return instances of Rx Observable that allow us to run network operations asynchronously
 */
interface CountryService {

    @GET("rest/v1/all")
    fun getCountries(): Single<List<Country>>

}

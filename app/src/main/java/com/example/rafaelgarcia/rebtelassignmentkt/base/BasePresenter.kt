package com.example.rafaelgarcia.rebtelassignmentkt.base

import io.reactivex.disposables.CompositeDisposable

/**
 * This Base class acts as the contract that describes the interactions between
 * the view and the presenter, also would handle to dispose the observables as needed.
 */
abstract class BasePresenter<V> {

    protected var view: V? = null
    protected val disposables = CompositeDisposable()

    /**
     * Called when we are ready to use the view
     *
     * @param view The view to be interacting with
     */
    fun attachView(view: V) {
        this.view = view
    }

    /**
     * Called when the view has been destroyed
     */
    fun detachView() {
        //dispose all disposable
        disposables.clear()
        view = null
    }

    /**
     * Indicates if the view is available to use
     */
    val isViewAttached: Boolean get() = view != null

}

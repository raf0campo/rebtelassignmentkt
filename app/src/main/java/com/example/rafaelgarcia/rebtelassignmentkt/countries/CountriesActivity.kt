package com.example.rafaelgarcia.rebtelassignmentkt.countries

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.example.rafaelgarcia.rebtelassignmentkt.R
import com.example.rafaelgarcia.rebtelassignmentkt.countries.view.CountriesFragment
import com.example.rafaelgarcia.rebtelassignmentkt.utils.replaceFragment
import kotlinx.android.synthetic.main.activity_countries.*

class CountriesActivity : Activity() {

    companion object {
        fun startCountriesActivity(context: Context) {
            val startIntent = Intent(context, CountriesActivity::class.java)
            context.startActivity(startIntent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_countries);
        replaceFragment(CountriesFragment(), countries_container.id)
    }
}

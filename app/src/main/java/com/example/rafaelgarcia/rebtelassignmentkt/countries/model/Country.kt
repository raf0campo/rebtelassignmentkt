package com.example.rafaelgarcia.rebtelassignmentkt.countries.model

import android.os.Parcel
import android.os.Parcelable

/**
 * Country object class model
 */
data class Country constructor(
        val name: String,
        val topLevelDomain: List<String>,
        val alpha2Code: String,
        val capital: String,
        val nativeName: String,
        val flagUrl: String) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.createStringArrayList(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        p0?.writeString(name);
        p0?.writeStringList(topLevelDomain);
        p0?.writeString(alpha2Code);
        p0?.writeString(capital);
        p0?.writeString(nativeName);
        p0?.writeString(flagUrl);
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Country> {
        override fun createFromParcel(parcel: Parcel): Country {
            return Country(parcel)
        }

        override fun newArray(size: Int): Array<Country?> {
            return arrayOfNulls(size)
        }
    }

}

package com.example.rafaelgarcia.rebtelassignmentkt.countries.presenter

import android.util.Log
import com.example.rafaelgarcia.rebtelassignmentkt.api.ApiManager
import com.example.rafaelgarcia.rebtelassignmentkt.base.BasePresenter
import com.example.rafaelgarcia.rebtelassignmentkt.countries.model.Country
import com.example.rafaelgarcia.rebtelassignmentkt.countries.view.CountriesView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class CountriesPresenter : BasePresenter<CountriesView>() {

    fun loadCountries() {
        view?.showLoading()
        val countryDisposable: Disposable = ApiManager.instance.getCountryService()
                .getCountries()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object: DisposableSingleObserver<List<Country>>() {
                    override fun onError(e: Throwable) {
                        Log.e(TAG, "There was an error : " + e.message)
                        view?.stopLoading()
                        view?.showError()
                    }

                    override fun onSuccess(result: List<Country>) {
                        view?.showCountries(result)
                        view?.stopLoading()
                    }
                })

        disposables.add(countryDisposable)

    }

    companion object {
        @JvmField
        val TAG: String = CountriesPresenter::class.java.simpleName
    }

}

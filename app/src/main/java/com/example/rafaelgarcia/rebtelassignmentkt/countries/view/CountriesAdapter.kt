package com.example.rafaelgarcia.rebtelassignmentkt.countries.view

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.rafaelgarcia.rebtelassignmentkt.R
import com.example.rafaelgarcia.rebtelassignmentkt.countries.model.Country
import kotlinx.android.synthetic.main.countries_list_item.view.*

class CountriesAdapter(val countries: List<Country>): RecyclerView.Adapter<CountriesAdapter.CountryViewHolder>() {

    override fun onBindViewHolder(holder: CountryViewHolder?, position: Int) {
        holder?.bindItems(countries[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): CountryViewHolder {
        val v = LayoutInflater.from(parent?.context).inflate(R.layout.countries_list_item, parent, false)
        return CountryViewHolder(v)
    }

    override fun getItemCount(): Int { return countries.size }

    class CountryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(country: Country) {

            itemView.tv_country_name_id.text = country.name
            itemView.tv_country_capital_id.text=country.capital
            itemView.tv_country_native_id.text = country.nativeName
        }
    }

}

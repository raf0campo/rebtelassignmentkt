package com.example.rafaelgarcia.rebtelassignmentkt.countries.view

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import com.example.rafaelgarcia.rebtelassignmentkt.R
import com.example.rafaelgarcia.rebtelassignmentkt.base.BaseFragment
import com.example.rafaelgarcia.rebtelassignmentkt.countries.model.Country
import com.example.rafaelgarcia.rebtelassignmentkt.countries.presenter.CountriesPresenter
import kotlinx.android.synthetic.main.fragment_countries.*

class CountriesFragment : BaseFragment(), CountriesView {

    private val countriesPresenter: CountriesPresenter = CountriesPresenter()

    private val countriesRv: RecyclerView by lazy {
        countries_list
    }

    private val loading: ProgressBar by lazy {
        pb_loading
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_countries
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        countriesPresenter.attachView(this)
        countriesPresenter.loadCountries()
    }

    override fun onDestroyView() {
        countriesPresenter.detachView()
        super.onDestroyView()
    }

    override fun showCountries(countries: List<Country>) {
        Log.e(TAG, "showCountries() "  + countries.toString())
        countriesRv.adapter = CountriesAdapter(countries)
    }

    override fun showError() {
        Log.e(Companion.TAG, "Show Error : ")
    }

    override fun showLoading() {
        loading.visibility = View.VISIBLE
    }

    override fun stopLoading() {
        loading.visibility = View.INVISIBLE
    }

    companion object {
        @JvmField
        val TAG: String = CountriesFragment::class.java.simpleName
    }

}

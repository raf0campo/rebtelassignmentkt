package com.example.rafaelgarcia.rebtelassignmentkt.countries.view

import com.example.rafaelgarcia.rebtelassignmentkt.countries.model.Country

interface CountriesView {

    fun showCountries(countries: List<Country>)

    fun showError()

    fun showLoading()

    fun stopLoading()

}

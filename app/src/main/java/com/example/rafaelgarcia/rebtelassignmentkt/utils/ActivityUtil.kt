package com.example.rafaelgarcia.rebtelassignmentkt.utils

import android.app.Activity
import android.app.Fragment
import android.app.FragmentManager
import android.app.FragmentTransaction

inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) {
    beginTransaction().func().commit()
}

fun Activity.addFragment(fragment: Fragment, frameId: Int) {
    fragmentManager.inTransaction { add(frameId, fragment) }
}

fun Activity.replaceFragment(fragment: Fragment, frameId: Int) {
    fragmentManager.inTransaction { replace(frameId, fragment) }
}

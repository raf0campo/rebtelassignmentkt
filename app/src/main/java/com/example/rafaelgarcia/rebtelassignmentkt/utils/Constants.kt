package com.example.rafaelgarcia.rebtelassignmentkt.utils

class Constants private constructor() {

    companion object {
        
        @JvmStatic
        val WARNING_DIALOG_TITLE = "Exit"
        @JvmStatic
        val WARNING_DIALOG_MESSAGE = "Are you sure? :("
    }

}

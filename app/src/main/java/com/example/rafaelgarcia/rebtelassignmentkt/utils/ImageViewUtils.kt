package com.example.rafaelgarcia.rebtelassignmentkt.utils

import android.widget.ImageView
import com.squareup.picasso.Picasso

fun ImageView.loadImageWithUrl(url: String) {
    Picasso.with(context).load(url).into(this)
}

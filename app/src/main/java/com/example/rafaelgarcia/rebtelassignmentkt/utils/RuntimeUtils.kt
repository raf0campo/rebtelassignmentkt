package com.example.rafaelgarcia.rebtelassignmentkt.utils

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import com.example.rafaelgarcia.rebtelassignmentkt.R

class RuntimeUtils private constructor () {

    companion object {

        /**
         * Display a warning dialog to inform the user is about to exit the app
         * @param context the context from where this method is called
         * @param title   the label required for the title
         * @param message the message you want to how the user
         */
        fun displayWarningDialog(context: Context, title: String, message: String) {

            val builder = AlertDialog.Builder(context)

            builder.setTitle(title)
            builder.setMessage(message)

            builder.setPositiveButton(
                    context.resources.getString(R.string.dialog_network_error_positive), { dialog, _ -> // _ is which, but never used, can be renamed to _
                        dialog.dismiss()
                        (context as Activity).finish()
                    }
            )

            builder.setNegativeButton(
                    context.resources.getString(R.string.dialog_network_error_negative), { dialog, _ -> // _ is which, but never used, can be renamed to _
                        dialog.dismiss()
                    }
            )

            val alert = builder.create()
            alert.show()
        }

    }

}


